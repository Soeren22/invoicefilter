package de.szut.mocking;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class InvoiceFilter {
	private InvoiceRepository repository;
	
	public InvoiceFilter(InvoiceRepository repository){
		this.repository = repository;
	}

	public List<Invoice> filter() {
		
		List<Invoice>filtered = new ArrayList<Invoice>();
		
		for(Invoice invoice : repository.all()) {
			if (invoice.getAmount() > 2000) filtered.add(invoice);
			else if (invoice.getAmount() < 2000 && invoice.getCustomer().equals("MICROSOFT")) filtered.add(invoice);
			else if (invoice.getDate().get(Calendar.YEAR) < 1999) filtered.add(invoice);
		}
		
		return filtered;
	}
}
